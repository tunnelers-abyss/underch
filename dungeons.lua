local SECRETSEED = 0xdeadbeef

local function content_id(name)
	local result = minetest.get_content_id("air")
	if minetest.registered_items[name] then
		result = minetest.get_content_id(name)
	end
	return result
end

local c_air = content_id("air")
local c_torch = content_id("default:torch")
local c_ladder = content_id("default:ladder_wood")
local c_chest = content_id("default:chest")
local c_lava = minetest.get_content_id("default:lava_source")


local function swap(array, idx1, idx2)
	local tmp = array[idx1]
	array[idx1] = array[idx2]
	array[idx2] = tmp
end

local function shuffle(array)
	local arr_len = #array
	for i = 1, arr_len - 1 do
		swap(array, i, math.random(i, arr_len))
	end
end

local function maparea(datas, area, cmin, cmax, fun)
	assert(#cmin == 3)
	assert(#cmax == 3)
	local ai = area:index(cmin[1], cmin[2], cmin[3])
	local xw = cmax[1] - cmin[1] + 1
	local yw = cmax[2] - cmin[2] + 1
	local zw = cmax[3] - cmin[3] + 1

	for _ = 1, zw do
		for _ = 1, yw do
			for _ = 1, xw do
				fun(datas, ai)
				ai = ai + 1;
			end
			ai = ai - xw + area.ystride
		end
		ai = ai - yw * area.ystride + area.zstride
	end
end

underch.base_stone = {
	[1] = "dolomite",
	[2] = "limestone",
	[3] = "schist",
	[4] = "andesite",
	[5] = "phylite",
	[6] = "quartzite",
	[7] = "amphibolite",
	[8] = "slate",
	[9] = "gneiss",
	[10] = "phonolite",
	[11] = "aplite",
	[12] = "basalt",
	[13] = "diorite",
	[14] = "pegmatite",
	[15] = "granite",
	[16] = "gabbro",
	[17] = "dolomite",
	[18] = "limestone",
	[19] = "amphibolite",
	[20] = "vindesite",
	[21] = "phylite",
	[22] = "phonolite",
	[23] = "schist",
	[24] = "slate",
	[25] = "diorite",
	[26] = "phonolite",
	[27] = "basalt",
	[28] = "green_slimestone",
	[29] = "sichamine",
	[30] = "sichamine",
	[31] = "granite",
	[32] = "andesite",
	[33] = "marble",
	[34] = "phonolite",
	[35] = "schist",
	[36] = "dark_vindesite",
	[37] = "phylite",
	[38] = "omphyrite",
	[39] = "pegmatite",
	[40] = "purple_slimestone",
	[41] = "gneiss",
	[42] = "granite",
	[43] = "basalt",
	[44] = "red_slimestone",
	[45] = "sichamine",
	[46] = "diorite",
	[47] = "andesite",
	[48] = "gabbro",
	[49] = "omphyrite",
	[50] = "afualite",
	[51] = "afualite",
	[52] = "gneiss",
	[53] = "hektorite",
	[54] = "basalt",
	[55] = "sichamine",
	[56] = "granite",
	[57] = "peridotite",
	[58] = "hektorite",
	[59] = "afualite",
	[60] = "emutite",
	[61] = "peridotite",
	[62] = "obscurite"
}

local loots = {"default:diamond", "default:gold_ingot", "default:steel_ingot", "default:mese", "default:copper_ingot", "default:copper_lump", "default:iron_lump", "default:gold_lump", "default:tin_lump", "default:wood", "default:tree", "default:book", "default:sword_diamond", "default:sword_wood", "default:sword_stone", "default:stick", "default:sword_bronze", "default:pick_diamond", "default:pick_wood", "default:pick_stone", "default:pick_steel", "default:pick_bronze", "default:axe_diamond", "default:axe_wood", "default:axe_stone", "default:axe_steel", "default:axe_bronze", "default:shovel_diamond", "default:shovel_wood", "default:shovel_stone", "default:shovel_steel", "default:shovel_bronze"}

local chances = {10, 10, 10, 10, 10, 10, 10, 10, 10, 20, 20, 5, 30, 30, 30, 5, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 36, 30, 30, 30, 30}

local quantities = {10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 3, 1, 1, 1, 15, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}

local function add_lootitem(name, chance, quantity)
    assert(type(name) == "string", name.." is not a string")
    assert(type(quantity) == "number", quantity.." is not a number")
    assert(type(chance) == "number", chance.." is not a number")
    table.insert(loots, name)
    table.insert(quantities, quantity)
    table.insert(chances, chance)
end

if minetest.get_modpath("xtraores") then
    add_lootitem("xtraores:geminitinum_lump", 100, 3)
    add_lootitem("xtraores:osmium_lump", 10, 10)
    add_lootitem("xtraores:rarium_lump", 20, 10)
    add_lootitem("xtraores:titanium_lump", 20, 10)
    add_lootitem("xtraores:adamantite_lump", 15, 10)
    add_lootitem("xtraores:cobalt_lump", 15, 10)
    add_lootitem("xtraores:geminitinum_ingot", 30, 10)
    add_lootitem("xtraores:osmium_ingot", 10, 10)
    add_lootitem("xtraores:rarium_ingot", 10, 10)
    add_lootitem("xtraores:titanium_ingot", 10, 10)
    add_lootitem("xtraores:adamantite_ingot", 10, 10)
    add_lootitem("xtraores:cobalt_ingot", 10, 10)
end

if minetest.get_modpath("underch") then
    add_lootitem("underch:saphire", 15, 10)
    add_lootitem("underch:ruby", 15, 10)
    add_lootitem("underch:emerald", 15, 10)
end

if minetest.get_modpath("africaplants") then
    add_lootitem("africaplants:cortinarius_violaceus", 50, 2)
end

if minetest.get_modpath("currency") then
    add_lootitem("currency:buck_10", 10, 8)
end

if minetest.get_modpath("cottages") then
    add_lootitem("cottages:straw_bale", 10, 40)
end

if minetest.get_modpath("natal") then
    add_lootitem("natal:natal_cycad_sapling", 20, 10)
    add_lootitem("natal:prince_cycad_sapling", 20, 10)
    add_lootitem("natal:woodcycad_sapling", 20, 10)
end

if minetest.get_modpath("technic") then
    add_lootitem("technic:uranium_lump", 10, 10)
    add_lootitem("technic:zinc_lump", 10, 10)
    add_lootitem("technic:zinc_ingot", 10, 10)
end

if minetest.get_modpath("octu") then
    add_lootitem("octu:octu", 15, 8)
end

if minetest.get_modpath("xdecor") then
    add_lootitem("xdecor:cobweb", 5, 16)
end

if minetest.get_modpath("moreores") then
    add_lootitem("moreores:silver_lump", 10, 8)
end

if minetest.get_modpath("lavastuff") then
    add_lootitem("lavastuff:orb", 10, 8)
end

if minetest.get_modpath("coronavirus") then
    add_lootitem("default:coronavirus", 5, 9000)
end

if minetest.get_modpath("elements") then
    c_torch = content_id("default:unlit_torch")
end

if #chances ~= #quantities or #chances ~= #loots then
    assert(false, "list lengths don't match: #loots: "
        ..#loots.." #chance: "..#chances.." #quantities: "..#quantities)
end

underch.dungeon_postops = {
    ["populate_chest"] = function(pos)
        local pos = {x=pos[1], y=pos[2], z=pos[3]}
        minetest.registered_nodes["default:chest"].on_construct(pos)
        local inv = minetest.get_inventory({type="node", pos=pos})
        for i=1,#chances,1 do
           if math.random(1, chances[i]) == 1 then
             local randomnumber = math.random(1, quantities[i])
             inv:add_item("main", ItemStack(loots[i].." "..randomnumber))
           end
        end
    end,
    ["torch"] = function(pos)
        -- torches are the same as cigarettes right, so ... they should defy gravity as well
    end
}

underch.dungeon_roomstyle = {
	[1] = {
		-- mold
		combine_cost = 0.5,
		on_walls = true,
		on_ceiling = true,
		on_floor = true,
		node = content_id("underch:mould"),
		rotate = true,
	},
	[2] = {
		-- cobweb
		on_walls = true,
		on_ceiling = true,
		on_floor = true,
		combine_cost = 0.5,
		node = content_id("xdecor:cobweb"),
	},
	[3] = {
		-- chest
		combine_cost = 0.0,
		likelihood = 0.4,
        on_floor = function(datas, air_mask, area, v)
            local x = math.random(v[1][1], v[2][1])
            local z = math.random(v[1][3], v[2][3])
            local y = v[1][2]
            local index = area:index(x, y, z)
            if datas[1][index] ~= air then
                datas[1][index] = c_chest
                air_mask[index] = true
                if type(datas.postops.populate_chest) ~= "table" then
                    datas.postops.populate_chest = {}
                end
                datas.postops.populate_chest[#datas.postops.populate_chest + 1] = {x, y, z}
            end
        end,
	},
	[4] = {
		-- torch
		combine_cost = 0,
		on_walls = function(datas, air_mask, area, v)
			local param2
			if v[3] == "-Z" then
				param2 = 4
			elseif v[3] == "+Z" then
				param2 = 5
			elseif v[3] == "-X" then
				param2 = 2
			elseif v[3] == "+X" then
				param2 = 3
            end
            if type(datas.postops.torch) ~= "table" then
                datas.postops.torch = {}
            end
			local stride = 3
			local y = v[1][2] + 1
			if v[3] == "-Z" or v[3] == "+Z" then
				local z = v[1][3]
				for x = v[1][1] + math.random(0, stride-1), v[2][1], stride do
                    local index = area:index(x,y,z)
                    if datas[1][index] ~= c_air then
                        datas[1][index] = c_torch
                        datas[2][index] = param2
                        air_mask[index] = true
                        datas.postops.torch[#datas.postops.torch + 1] = {x, y, z}
                    end
				end
			elseif v[3] == "-X" or v[3] == "+X" then
				local x = v[1][1]
				for z = v[1][3] + math.random(0, stride-1), v[2][3], stride do
					local index = area:index(x,y,z)
					datas[1][index] = c_torch
					datas[2][index] = param2
                    air_mask[index] = true
                    datas.postops.torch[#datas.postops.torch + 1] = {x, y, z}
				end
			end
		end,
	},
	[5] = {
		-- dead_bush
		combine_cost = 0.5,
		on_floor = true,
		node = content_id("underch:dead_bush"),
	},
	[6] = {
		-- fiery vine
		combine_cost = 0.5,
		on_walls = true,
		node = content_id("underch:fiery_vine"),
		rotate = true,
	},
	[7] = {
		-- underground vine
		combine_cost = 0.5,
		on_walls = true,
		node = content_id("underch:underground_vine"),
		rotate = true,
	},
	[8] = {
		-- torchberries
		combine_cost = 0.5,
		on_ceiling = true,
		node = content_id("underch:torchberries"),
		rotate = true,
	},
	[9] = {
		-- green mushroom
		combine_cost = 0.5,
		on_ceiling = true,
		node = content_id("underch:green_mushroom"),
	},
	[10] = {
		-- lava
		combine_cost = 0.3,
        on_volume = function(datas, air_mask, area, v)
            local set_node = function(datas, index)
                if air_mask[index] ~= true and datas[1][index] ~= c_air then
                    air_mask[index] = true
                    datas[1][index] = c_lava
				end
			end
			maparea(datas, area, v[1], v[2], set_node)
		end
    },
    [11] = {
        -- tarantula
        combine_cost = 0.05,
        on_floor = function(datas, air_mask, area, v)
            minetest.add_entity({
                x = math.random(v[1][1], v[2][1]),
                y = math.random(v[1][2], v[2][2]),
                z = math.random(v[1][3], v[2][3]),
            }, "petz:tarantula")
        end
    }
}

underch.dungeon_genrules = {
	["dolomite"] = {
		wall = {
			{
				node = content_id("underch:dolomite_cobble"),
				p = 0.9
			},
			{
				node = content_id("underch:dolomite_mossy_cobble"),
				p = 0.5,
			},
		},
		styles = {1, 2, 3, 4, 5, 6, 7, 8, 9, 11},
	},
	["limestone"] = {
		wall = {
			{
				node = content_id("underch:limestone_cobble"),
				p = 0.9
			},
			{
				node = content_id("underch:limestone_mossy_cobble"),
				p = 0.5,
			},
		},
		styles = {1, 2, 3, 4, 5, 6, 7, 8, 9, 11},
	},
	["schist"] = {
		wall = {
			{
				node = content_id("underch:schist_cobble"),
				p = 0.9
			},
			{
				node = content_id("underch:schist_mossy_cobble"),
				p = 0.5,
			},
		},
		styles = {1, 2, 3, 4, 5, 6, 7, 8, 9, 11},
	},
	["andesite"] = {
		wall = {
			{
				node = content_id("underch:andesite_cobble"),
				p = 0.9
			},
			{
				node = content_id("underch:andesite_mossy_cobble"),
				p = 0.5,
			},
		},
		styles = {1, 2, 3, 4, 5, 6, 7, 8, 9, 11},
	},
	["phylite"] = {
		wall = {
			{
				node = content_id("underch:phylite_cobble"),
				p = 0.9
			},
			{
				node = content_id("underch:phylite_mossy_cobble"),
				p = 0.5,
			},
		},
		styles = {1, 2, 3, 4, 5, 6, 7, 8, 9, 11},
	},
	["quartzite"] = {
		wall = {
			{
				node = content_id("underch:quartzite_cobble"),
				p = 0.9
			},
			{
				node = content_id("underch:quartzite_mossy_cobble"),
				p = 0.5,
			},
		},
		styles = {1, 2, 3, 4, 5, 6, 7, 8, 9, 11},
	},
	["amphibolite"] = {
		wall = {
			{
				node = content_id("underch:amphibolite_cobble"),
				p = 0.9
			},
			{
				node = content_id("underch:amphibolite_mossy_cobble"),
				p = 0.5,
			},
		},
		styles = {1, 2, 3, 4, 5, 6, 7, 8, 9, 11},
	},
	["slate"] = {
		wall = {
			{
				node = content_id("underch:slate_cobble"),
				p = 0.9
			},
			{
				node = content_id("underch:slate_mossy_cobble"),
				p = 0.5,
			},
		},
		styles = {1, 2, 3, 4, 5, 6, 7, 8, 9, 11},
	},
	["gneiss"] = {
		wall = {
			{
				node = content_id("underch:gneiss_cobble"),
				p = 0.9
			},
			{
				node = content_id("underch:gneiss_mossy_cobble"),
				p = 0.5,
			},
		},
		styles = {1, 2, 3, 4, 5, 6, 7, 8, 9, 11},
	},
	["phonolite"] = {
		wall = {
			{
				node = content_id("underch:phonolite_cobble"),
				p = 0.9
			},
			{
				node = content_id("underch:phonolite_mossy_cobble"),
				p = 0.5,
			},
		},
		styles = {1, 2, 3, 4, 5, 6, 7, 8, 9, 11},
	},
	["aplite"] = {
		wall = {
			{
				node = content_id("underch:aplite_cobble"),
				p = 0.9
			},
			{
				node = content_id("underch:aplite_mossy_cobble"),
				p = 0.5,
			},
		},
		styles = {1, 2, 3, 4, 5, 6, 7, 8, 9, 11},
	},
	["basalt"] = {
		wall = {
			{
				node = content_id("underch:basalt_cobble"),
				p = 0.9
			},
			{
				node = content_id("underch:basalt_mossy_cobble"),
				p = 0.5,
			},
		},
		styles = {1, 2, 3, 4, 5, 6, 7, 8, 9, 11},
	},
	["diorite"] = {
		wall = {
			{
				node = content_id("underch:diorite_cobble"),
				p = 0.9
			},
			{
				node = content_id("underch:diorite_mossy_cobble"),
				p = 0.5,
			},
		},
		styles = {1, 2, 3, 4, 5, 6, 7, 8, 9, 11},
	},
	["pegmatite"] = {
		wall = {
			{
				node = content_id("underch:pegmatite_cobble"),
				p = 0.9
			},
			{
				node = content_id("underch:pegmatite_mossy_cobble"),
				p = 0.5,
			},
		},
		styles = {1, 2, 3, 4, 5, 6, 7, 8, 9, 11},
	},
	["granite"] = {
		wall = {
			{
				node = content_id("underch:granite_cobble"),
				p = 0.9
			},
			{
				node = content_id("underch:granite_mossy_cobble"),
				p = 0.5,
			},
		},
		styles = {1, 2, 3, 4, 5, 6, 7, 8, 9, 11},
	},
	["gabbro"] = {
		wall = {
			{
				node = content_id("underch:gabbro_cobble"),
				p = 0.9
			},
			{
				node = content_id("underch:gabbro_mossy_cobble"),
				p = 0.5,
			},
		},
		styles = {1, 2, 3, 4, 5, 6, 7, 8, 9, 11},
	},
	["vindesite"] = {
		wall = {
			{
				node = content_id("underch:vindesite_cobble"),
				p = 0.9
			},
			{
				node = content_id("underch:vindesite_mossy_cobble"),
				p = 0.5,
			},
		},
		styles = {1, 2, 3, 4, 5, 6, 7, 8, 9, 11},
	},
	["dark_vindesite"] = {
		wall = {
			{
				node = content_id("underch:dark_vindesite_cobble"),
				p = 0.9
			},
			{
				node = content_id("underch:dark_vindesite_mossy_cobble"),
				p = 0.5,
			},
		},
		styles = {1, 2, 3, 4, 5, 6, 7, 8, 9, 11},
	},
	["green_slimestone"] = {
		wall = {
			{
				node = content_id("underch:green_slimy_block"),
				p = 0.9
			},
		},
		styles = {1, 2, 3, 4, 5, 6, 7, 8, 9, 11},
	},
	["red_slimestone"] = {
		wall = {
			{
				node = content_id("underch:red_slimy_block"),
				p = 0.9
			},
		},
		styles = {1, 2, 3, 4, 5, 6, 7, 8, 9, 11},
	},
	["black_slimestone"] = {
		wall = {
			{
				node = content_id("underch:black_slimy_block"),
				p = 0.9
			},
		},
		styles = {1, 2, 3, 4, 5, 6, 7, 8, 9, 11},
	},
	["purple_slimestone"] = {
		wall = {
			{
				node = content_id("underch:purple_slimy_block"),
				p = 0.9
			},
		},
		styles = {1, 2, 3, 4, 5, 6, 7, 8, 9, 11},
	},
	["afualite"] = {
		wall = {
			{
				node = content_id("underch:afualite_cobble"),
				p = 0.9
			},
		},
		styles = {1, 2, 3, 4, 5, 6, 7, 8, 9, 11},
	},
	["hektorite"] = {
		wall = {
			{
				node = content_id("underch:hektorite_cobble"),
				p = 0.9
			},
		},
		styles = {1, 2, 3, 4, 5, 6, 7, 8, 9, 11},
	},
	["sichamine"] = {
		wall = {
			{
				node = content_id("underch:sichamine_cobble"),
				p = 0.9
			},
			{
				node = content_id("underch:sichamine_mossy_cobble"),
				p = 0.5,
			},
		},
		styles = {1, 2, 3, 4, 5, 6, 7, 8, 9, 11},
	},
	["emutite"] = {
		wall = {
			{
				node = content_id("underch:emutite_cobble"),
				p = 0.9
			},
		},
		styles = {1, 2, 3, 4, 5, 6, 7, 8, 9, 11},
	},
	["obscurite"] = {
		wall = {
			{
				node = content_id("underch:obscurite"),
				p = 0.9
			},
		},
		styles = {3, 10},
	},
	["omphyrite"] = {
		wall = {
			{
				node = content_id("underch:omphyrite_cobble"),
				p = 0.9
			},
		},
		styles = {1, 2, 3, 4, 5, 6, 7, 8, 9, 11},
	},
	["marble"] = {
		wall = {
			{
				node = content_id("underch:marble_cobble"),
				p = 0.9
			},
			{
				node = content_id("underch:marble_mossy_cobble"),
				p = 0.8,
			},
			{
				node = content_id("underch:shinestone"),
				p = 0.5
			},
			{
				node = content_id("default:goldblock")
			}
		},
		styles = {3, 4, 6},
    },
    ["peridotite"] = {
		wall = {
			{
				node = content_id("underch:peridotite_cobble"),
				p = 0.9
			},
		},
		styles = {1, 2, 3, 4, 5, 6, 7, 8, 9, 11},
	},
}

local function build_box(datas, area, cmin, cmax, set_node)
	assert(#cmin == 3)
	assert(#cmax == 3)
	-- XY walls
	maparea(datas, area,
		{cmin[1], cmin[2], cmin[3]},
		{cmax[1], cmax[2], cmin[3]}, set_node)
	maparea(datas, area,
		{cmin[1], cmin[2], cmax[3]},
		{cmax[1], cmax[2], cmax[3]}, set_node)
	-- XZ walls
	maparea(datas, area,
		{cmin[1], cmin[2], cmin[3]},
		{cmax[1], cmin[2], cmax[3]}, set_node)
	maparea(datas, area,
		{cmin[1], cmax[2], cmin[3]},
		{cmax[1], cmax[2], cmax[3]}, set_node)
	-- YZ walls
	maparea(datas, area,
		{cmin[1], cmin[2], cmin[3]},
		{cmin[1], cmax[2], cmax[3]}, set_node)
	maparea(datas, area,
		{cmax[1], cmin[2], cmin[3]},
		{cmax[1], cmax[2], cmax[3]}, set_node)
end

local function rotation_param2(normal)
	if normal == "+Z" then
		return 5
	elseif normal == "-Z" then
		return 4
	elseif normal == "+X" then
		return 3
	elseif normal == "-X" then
		return 2
	elseif normal == "+Y" then
		return 1
	elseif normal == "-Y" then
		return 0
	end
end

local function apply_style(datas, area, styleinfo, cmin, cmax, air_mask)
	local node = styleinfo.node

	local wall_areas = {}

	if styleinfo.on_walls then
		wall_areas[#wall_areas + 1] = {{cmin[1] + 1, cmin[2] + 1, cmin[3] + 1}, {cmax[1] - 1, cmax[2] - 1, cmin[3] + 1}, "+Z"}
		wall_areas[#wall_areas + 1] = {{cmin[1] + 1, cmin[2] + 1, cmin[3] + 1}, {cmin[1] + 1, cmax[2] - 1, cmax[3] - 1}, "+X"}

		wall_areas[#wall_areas + 1] = {{cmax[1] - 1, cmin[2] + 1, cmin[3] + 1}, {cmax[1] - 1, cmax[2] - 1, cmax[3] - 1},"-X"}
		wall_areas[#wall_areas + 1] = {{cmin[1] + 1, cmin[2] + 1, cmax[3] - 1}, {cmax[1] - 1, cmax[2] - 1, cmax[3] - 1},"-Z"}
	end

	local set_node_fun = function(param2)
		return function(datas, index)
			if datas[1][index] ~= c_air and math.random() < 0.2 then
				datas[1][index] = node
				if param2 ~= nil then
					datas[2][index] = param2
				end
				air_mask[index] = true
			end
		end
	end

	for i,v in ipairs(wall_areas) do
		if type(styleinfo.on_walls) == "function" then
			styleinfo.on_walls(datas, air_mask, area, v)
		else
			maparea(datas, area, v[1], v[2], set_node_fun(styleinfo.rotate and rotation_param2(v[3])))
		end
	end

    if styleinfo.on_ceiling then
        local v = {{cmax[1] + 1, cmax[2] - 1, cmin[3] + 1}, {cmax[1] - 1, cmax[2] - 1, cmax[3] - 1}, "-Y"}
		if type(styleinfo.on_ceiling) == "function" then
			styleinfo.on_ceiling(datas, air_mask, area, v)
		else
			maparea(datas, area, v[1], v[2], set_node_fun(styleinfo.rotate and rotation_param2(v[3])))
		end
	end

    if styleinfo.on_floor then
        local v = {{cmin[1] + 1, cmin[2] + 1, cmin[3] + 1}, {cmax[1] - 1, cmin[2] + 1, cmax[3] - 1}, "+Y"}
		if type(styleinfo.on_floor) == "function" then
			styleinfo.on_floor(datas, air_mask, area, v)
		else
			maparea(datas, area, v[1], v[2], set_node_fun(styleinfo.rotate and rotation_param2(v[3])))
		end
	end

	if styleinfo.on_volume then
		if type(styleinfo.on_volume) == "function" then
			local v = {{cmin[1] + 1, cmin[2] + 1, cmin[3] + 1}, {cmax[1] - 1, cmax[2] - 1, cmax[3] - 1}, nil}
			styleinfo.on_volume(datas, air_mask, area, v)
		else
			maparea(datas, area, v[1], v[2], set_node_fun(nil))
		end
	end
end

local function furnish_room(datas, area, geninfo, cmin, cmax, air_mask)
	local available_styles = underch.dungeon_roomstyle
	local accepted_styles = geninfo.styles
	local budget = 1

	local totalweights = 0
	for i,v in ipairs(accepted_styles) do
		totalweights = totalweights + (available_styles[v].likelihood or 1)
	end

	while budget > 0 do
		local num = math.random() * totalweights
		local choice = 0
		while num > 0 do
			choice = choice + 1
			num = num - (available_styles[accepted_styles[choice]].likelihood or 1)
		end
		assert(choice >= 1 and choice <= #accepted_styles)
		--print("apply ", minetest.get_name_from_content_id(available_styles[choice].node))
		apply_style(datas, area, available_styles[accepted_styles[choice]], cmin, cmax, air_mask)
		budget = budget - available_styles[accepted_styles[choice]].combine_cost
	end
end

-- direction is a signed number of 1,2,3 corresponding to the axis X,Y,Z, sign has direct correspondence on the axis
-- 0 means center
-- bitmap is a table using
local function build_room(datas, area, walldb, bitmap, attachment, direction, set_node, geninfo, branch_level)
    if #walldb > 128 or branch_level >= 10 then
		walldb[#walldb] = nil
		return
	end

	local cmin = {
		attachment[1] - math.random(1, 5),
		attachment[2] - math.random(1, 3),
		attachment[3] - math.random(1, 5)
	}
	local cmax = {
		attachment[1] + math.random(1, 5),
		attachment[2] + math.random(2, 3),
		attachment[3] + math.random(1, 5)
	}
	local delta;
	if direction ~= 0 then
		if direction < 0 then
			direction = -direction
			delta = attachment[direction] - cmax[direction]
		else
			delta = attachment[direction] - cmin[direction]
		end
		cmin[direction] = cmin[direction] + delta
		cmax[direction] = cmax[direction] + delta
	end


	-- candidate coordinates for corners have now been calculated
	-- now determine whether the space is already used and abort if so

	local divisor = 1

	-- calculates coordinate on a grid decimated by divisor
	local function fd(num)
		-- there might be a better solution to this
		while math.floor(num % divisor) ~= 0 do
			num = num - 1
		end
		return num
	end

	local lowbound = {x = fd(cmin[1]), y = fd(cmin[2]), z = fd(cmin[3])}
	local highbound = {x = fd(cmax[1]), y = fd(cmax[2]), z = fd(cmax[3])}

	--check if room is within the fetched voxel manipulator area
	if not area:containsp(lowbound)
		or not area:containsp(highbound) then
		walldb[#walldb] = nil
		return
	end

	-- abort if space is already occupied
	for z = lowbound.z, highbound.z, divisor do
		for y = lowbound.y, highbound.y, divisor do
			for x = lowbound.x, highbound.x, divisor do
				if bitmap[area:index(x, y, z)] then
					walldb[#walldb] = nil
					return
				end
			end
		end
	end

	-- space is available, now mark it as occupied
	for z = lowbound.z + 1, highbound.z - 1, divisor do
		for y = lowbound.y + 1, highbound.y - 1, divisor do
			for x = lowbound.x + 1, highbound.x - 1, divisor do
				bitmap[area:index(x, y, z)] = true
			end
		end
	end

	if #walldb > 0 and direction ~= 0 then
		-- store our connecting wall
		walldb[#walldb].to = {}
		for i = 1, 3 do
			if i == direction then
				assert(attachment[i] ~= nil)
				walldb[#walldb].to[i] = attachment[i]
				walldb[#walldb].to[i + 3] = attachment[i]
			else
				assert(cmin[i] ~= nil)
				assert(cmax[i] ~= nil)
				walldb[#walldb].to[i] = cmin[i]
				walldb[#walldb].to[i + 3] = cmax[i]
			end
		end
		if walldb[#walldb].to[2] == cmax[2] then
			walldb[#walldb].to[2] = cmin[2]
		end
		if walldb[#walldb].to[5] == cmax[5] then
			walldb[#walldb].to[5] = cmin[5]
		end
	end

	build_box(datas, area, cmin, cmax, set_node)

	local air_mask = {}

	furnish_room(datas, area, geninfo, cmin, cmax, air_mask)

	local cmin_inside = {cmin[1]+1, cmin[2]+1, cmin[3]+1}
	local cmax_inside = {cmax[1]-1, cmax[2]-1, cmax[3]-1}
	local set_air = function(datas, index)
		if not air_mask[index] then
			datas[1][index] = c_air
		end
	end


	-- clear out the inside
	maparea(datas, area, cmin_inside, cmax_inside, set_air)

	air_mask = nil


	-- go to each direction with some chance and try to construct another room there
	-- a description of our connecting wall will be appended to walldb, the callee shall attach its own to the entry

	-- to avoid directional bias all directions are shuffled
	local funs = {}
	local probability = 1/(branch_level + 2)

	if math.random() < probability then -- +X
		funs[#funs + 1] = function()
			walldb[#walldb + 1] = {from = {cmax[1], cmin[2], cmin[3], cmax[1], cmax[2], cmax[3]}}
			build_room(datas, area, walldb, bitmap, {
					cmax[1] + math.random(0, 3),
					math.floor((cmin[2] + cmax[2]) / 2) + math.random(-5, 5),
					math.floor((cmin[3] + cmax[3]) / 2) + math.random(-5, 5),
				}, 1, set_node, geninfo, branch_level + 1)
		end
	end
	if math.random() < probability then -- -X
		funs[#funs + 1] = function()
			walldb[#walldb + 1] = {from = {cmin[1], cmin[2], cmin[3], cmin[1], cmax[2], cmax[3]}}
			build_room(datas, area, walldb, bitmap, {
					cmin[1] - math.random(0, 3),
					math.floor((cmin[2] + cmax[2]) / 2) + math.random(-5, 5),
					math.floor((cmin[3] + cmax[3]) / 2) + math.random(-5, 5),
				}, -1, set_node, geninfo, branch_level + 1)
		end
	end
	if math.random() < probability then -- +Y
		funs[#funs + 1] = function()
			walldb[#walldb + 1] = {from = {cmin[1], cmin[2], cmin[3], cmax[1], cmin[2], cmax[3]}}
			build_room(datas, area, walldb, bitmap, {
					math.floor((cmin[1] + cmax[1]) / 2) + math.random(-5, 5),
					cmax[2] + math.random(0, 3),
					math.floor((cmin[3] + cmax[3]) / 2) + math.random(-5, 5),
				}, 2, set_node, geninfo, branch_level + 1)
		end
	end
	if math.random() < probability then -- -Y
		funs[#funs + 1] = function()
			walldb[#walldb + 1] = {from = {cmin[1], cmin[2], cmin[3], cmax[1], cmin[2], cmax[3]}}
			build_room(datas, area, walldb, bitmap, {
					math.floor((cmin[1] + cmax[1]) / 2) + math.random(-5, 5),
					cmin[2] - math.random(0, 3),
					math.floor((cmin[3] + cmax[3]) / 2) + math.random(-5, 5),
				}, -2, set_node, geninfo, branch_level + 1)
		end
	end
	if math.random() < probability then -- +Z
		funs[#funs + 1] = function()
			walldb[#walldb + 1] = {from = {cmin[1], cmin[2], cmax[3], cmax[1], cmax[2], cmax[3]}}
			build_room(datas, area, walldb, bitmap, {
					math.floor((cmin[1] + cmax[1]) / 2) + math.random(-5, 5),
					math.floor((cmin[2] + cmax[2]) / 2) + math.random(-5, 5),
					cmax[3] + math.random(0, 3),
				}, 3, set_node, geninfo, branch_level + 1)
		end
	end
	if math.random() < probability then -- -Z
		funs[#funs + 1] = function()
			walldb[#walldb + 1] = {from = {cmin[1], cmin[2], cmin[3], cmax[1], cmax[2], cmin[3]}}
			build_room(datas, area, walldb, bitmap, {
					math.floor((cmin[1] + cmax[1]) / 2) + math.random(-5, 5),
					math.floor((cmin[2] + cmax[2]) / 2) + math.random(-5, 5),
					cmin[3] - math.random(0, 3),
				}, -3, set_node, geninfo, branch_level + 1)
		end
	end
	shuffle(funs)
	while true do
		local funs_len = #funs
		if funs_len == 0 then
			break;
		end
		funs[funs_len]()
		funs[funs_len] = nil
	end
end

function steps_connect(datas, area, from, to, set_node)
	local x_inc
	local y_inc
	local z_inc

	local x_steps = math.floor(from[1]-to[1])
	local y_steps = math.floor(from[2]-to[2])
	local z_steps = math.floor(from[3]-to[3])

	if x_steps < 0 then
		x_inc = 1
		x_steps = -x_steps
	elseif x_steps > 0 then
		x_inc = -1
	end

	if y_steps < 0 then
		y_inc = 1
		y_steps = -y_steps
	elseif y_steps > 0 then
		y_inc = -1
	end

	if z_steps < 0 then
		z_inc = 1
		z_steps = -z_steps
	elseif z_steps > 0 then
		z_inc = -1
	end

	local set_air = function(datas, index)
		datas[1][index] = c_air
	end

	local set_ladder = function(datas, index)
		datas[1][index] = c_ladder
		datas[2][index] = 5
	end

	local hv = false
	local xz = false

	local clear_path = function()
		maparea(datas, area, from, {from[1], from[2] + 1, from[3]}, set_air)
		maparea(datas, area, {from[1] - 1, from[2] - 1, from[3] - 1}, {from[1] + 1, from[2] + 2, from[3] + 1}, set_node)
	end

	local x_step = function()
		from[1] = from[1] + x_inc
		x_steps = x_steps - 1
		clear_path()
		assert(math.abs(math.floor(from[1]-to[1])) == x_steps)
	end

	local z_step = function()
		from[3] = from[3] + z_inc
		z_steps = z_steps - 1
		clear_path()
		assert(math.abs(math.floor(from[3]-to[3])) == z_steps)
	end

	local horizontal_step = function()
		if x_steps > 0 and z_steps > 0 then
			if xz then
				x_step()
			else
				z_step()
			end
			xz = not xz
		elseif x_steps > 0 then
			x_step()
		elseif z_steps > 0 then
			z_step()
		end
	end

	local vertical_step = function()
		from[2] = from[2] + y_inc
		y_steps = y_steps - 1
		clear_path()
		assert(math.abs(math.floor(from[2]-to[2])) == y_steps)
	end

	clear_path()

	if y_steps > (x_steps + z_steps) then
		while y_steps > 0 do
			from[2] = from[2] + y_inc
			y_steps = y_steps - 1
			assert(math.abs(math.floor(from[2]-to[2])) == y_steps)
			maparea(datas, area, from, {from[1], from[2] + 1, from[3]}, set_ladder)
			maparea(datas, area, {from[1] - 1, from[2] - 1, from[3] - 1}, {from[1] + 1, from[2] + 2, from[3] + 1}, set_node)
		end
	end

	while x_steps > 0 or y_steps > 0 or z_steps > 0 do
		if y_steps > 0 and (x_steps > 0 or z_steps > 0) then
			if hv then
				horizontal_step()
			else
				vertical_step()
			end
			hv = not hv
		elseif y_steps > 0 then
			vertical_step()
		elseif (x_steps > 0 or z_steps > 0) then
			horizontal_step()
		end
	end

	--print(math.floor(from[1]-to[1]), x_steps)
	--assert(math.floor(from[1]-to[1]) == 0)
	--print(math.floor(from[2]-to[2]), y_steps)
	--assert(math.floor(from[2]-to[2]) == 0)
	--print(math.floor(from[3]-to[3]), z_steps)
	--assert(math.floor(from[3]-to[3]) == 0)
end

function underch.dynamic.build_dungeon(pos, geninfo)
    -- initialize RNG from position to get deterministic dungeon
    math.randomseed(pos.x + SECRETSEED)
    math.randomseed(pos.y+math.random(-100000,100000))
    math.randomseed(pos.z+math.random(-100000,100000))

    local vm = minetest.get_voxel_manip()
	local e1, e2 = vm:read_from_map({x = pos.x - 64, y = pos.y - 64, z = pos.z - 64}, {x = pos.x + 64, y = pos.y + 64, z = pos.z + 64})
	local datas = {vm:get_data(), vm:get_param2_data(), ["postops"] = {}}
	local area = VoxelArea:new{MinEdge=e1, MaxEdge=e2}


	local set_node = function(datas, index)
		if datas[1][index] == c_air or datas[1][index] == c_ladder then
			return
		end
		local len = #geninfo.wall
		for i,v in ipairs(geninfo.wall) do
			if i == len then
				datas[1][index] = v.node
				break
			elseif (math.random() < v.p) then
				datas[1][index] = v.node
				break
			end
		end
	end

	local walldb = {}
	local bitmap = {}

    build_room(datas, area, walldb, bitmap, {pos.x, pos.y, pos.z}, 0, set_node, geninfo, 0)

	set_node = function(datas, index)
		if datas[1][index] == c_air or datas[1][index] == c_ladder or bitmap[index] then
			return
		end
		local len = #geninfo.wall
		for i,v in ipairs(geninfo.wall) do
			if i == len then
				datas[1][index] = v.node
				break
			elseif (math.random() < v.p) then
				datas[1][index] = v.node
				break
			end
		end
	end

	for k,v in pairs(walldb) do
		for i = 1, 6 do
			assert(v.from[i] ~= nil)
			assert(v.to[i] ~= nil)
		end

		steps_connect(datas, area, {
				math.floor((v.from[1] + v.from[4]) / 2),
				v.from[2] + 1,
				math.floor((v.from[3] + v.from[6]) / 2)
			}, {
				math.floor((v.to[1] + v.to[4]) / 2),
				v.to[2] + 1,
				math.floor((v.to[3] + v.to[6]) / 2)
			}, set_node)
	end

	vm:set_data(datas[1])
	vm:set_param2_data(datas[2])
    vm:write_to_map()

    -- do post-processing
    for v,k in pairs(datas.postops) do
        if type(underch.dungeon_postops[v]) == "function" then
            for _,data in pairs(k) do
                underch.dungeon_postops[v](data)
            end
        else
            print("could not find handler function for dungeon postop "..v)
        end
    end

    math.randomseed(os.time())
end

